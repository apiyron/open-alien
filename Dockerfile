FROM java:8

RUN wget -q https://services.gradle.org/distributions/gradle-3.3-bin.zip \
    && unzip gradle-3.3-bin.zip -d /opt


ENV GRADLE_HOME /opt/gradle-3.3
ENV PATH $PATH:/opt/gradle-3.3/bin
COPY ./start.sh /
ENV name default_name
ENV port 8080
ENV tag default_tag

WORKDIR     /open-alien

ADD         build.gradle    /open-alien/build.gradle

ADD         src             /open-alien/src

CMD gradle bootRun -Dspring.application.name=${name} -Dserver.port=${port} -Dspring.cloud.consul.discovery.tags=${tag}

#CMD  './start.sh shuttle';
#docker run --net="host" -e name=shuttle -e port=8090 --env tag=lso --rm -v "$PWD":/usr/src/project -w /usr/src/project shuttle