# Open alien project
**Цель:**   
&nbsp;&nbsp; Изучение Spring и микросервисной архитектуры по мотивам фильма Чужой  

**Результат:**   
&nbsp;&nbsp; Необходимо реализовать сервис корабля Nostromo, несколько аналогичных сервисов Shuttle.  
&nbsp;&nbsp; Корабль и шаттлы взаимодействуют друг с другом через Service Discovery (SD).  

**Nostromo:**    
&nbsp;&nbsp; Отслеживает статус шаттлов и обменивается приборной информацией через SD  
**Shuttle:**   
&nbsp;&nbsp; Шаттлы предоставляют методы кораблю для получения кораблём информации о состоянии шаттла.
# TODO LIST:
```
✓ Запустить Spring 
✓ Реализовать класс шаттл 
✓ Присвоение случайного имени шаттла при создании 
✓ Реализовать метод шаттла getName 

x Разобраться с зависимостями и состояниями
x Запустить более чем 1 экземпляр шаттла
x Прикрутить Consul
x Прикрутить Eureka
```
## Ветки
dev — разработка  
test — тестирование  
master — продакшн

# Запуск шаттлов
```bash
 ./start.sh %name% %port% %tag% 
 ```
 
#ELK stack 

###Установка

Download filebeat: https://www.elastic.co/downloads/beats/filebeat

Download logstash: https://www.elastic.co/downloads/logstash

Использовать можно что-то одно, можно оба сразу, разница https://www.elastic.co/guide/en/beats/filebeat/1.1/diff-logstash-beats.html

Download elasticsearch: https://www.elastic.co/downloads/elasticsearch

Doanload kibana: https://www.elastic.co/downloads/kibana

###Конфиг файлы 
filebeat.yml 
```bash
- input_type: log

  # Paths that should be crawled and fetched. Glob based paths.
  paths:
  #  - /var/log/*.log
    - /path/to/open-alien/logs/*.log
    - /path/to/nostromo/logs/*.log
    
    Elasticsearch output ------------------------------
    output.elasticsearch:
      # Array of hosts to connect to.
      hosts: ["localhost:9200"]
    
      # Optional protocol and basic auth credentials.
      #protocol: "https"
      #username: "elastic"
      #password: "changeme"
    
    #----------------------------- Logstash output --------------------------------
    output.logstash:
      # The Logstash hosts
      hosts: ["127.0.0.1:5043"]
```

logstash.conf (его нужно создать в папке с logstash)
```bash
input {
 beats {
        port => "5043"
    }  
  }

output {
elasticsearch {
        hosts => [ "localhost:9200" ]
    }
  stdout { codec => rubydebug }
}
```  
###Запуск
```bash
#filebeat
cd /path/to/filebeat-5.5.1-darwin-x86_64
sudo ./filebeat -e -c filebeat.yml -d "publish" -strict.perms=false

#logstash
cd /path/to/logstash-5.5.1
bin/logstash -f logstash.conf --config.test_and_exit  
bin/logstash -f logstash.conf --config.reload.automatic

#elasticsearch
cd /path/to/elasticsearch-5.5.1
./bin/elasticsearch

#kibana
cd /path/to/kibana-5.5.1-darwin-x86_64
cd bin
./kibana
```
Чекнуть localhost:5601

# Шпаргалка по Git

### Git global setup

```bash
git config --global user.name "%%USER NAME"
git config --global user.email "%%EMAIL"
```

### Create a new repository
```bash
git clone https://gitlab.com/dolgikh.lex/open-alien.git
cd open-alien
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Existing folder

```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/dolgikh.lex/open-alien.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Existing Git repository

```bash
cd existing_repo
git remote add origin https://gitlab.com/dolgikh.lex/open-alien.git
git push -u origin --all
git push -u origin --tags
```