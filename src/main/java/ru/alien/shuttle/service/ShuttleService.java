package ru.alien.shuttle.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.alien.shuttle.controller.ShuttleController;
import ru.alien.shuttle.model.NostromoResponse;
import ru.alien.shuttle.model.PlanetEntity;
import ru.alien.shuttle.model.WikiFeignClient;
import ru.alien.shuttle.model.WikiResponseModel;
import ru.alien.shuttle.repository.PlanetEntityRepository;



@Component
public class ShuttleService {

    @Value("#{systemProperties['spring.application.name']}")
    private String name;

    @Value("#{systemProperties['server.port']}")
    private String port;

    private static Logger logger = LoggerFactory.getLogger(ShuttleController.class);

    private PlanetEntityRepository planetEntityRepository;

    private RestTemplate restTemplate;

    private WikiFeignClient wikiFeignClient;

    private ObjectMapper objMpr;

    public ShuttleService(WikiFeignClient wikiFeignClient, PlanetEntityRepository planetEntityRepository, RestTemplate restTemplate, ObjectMapper objMpr) {
        this.planetEntityRepository = planetEntityRepository;
        this.restTemplate = restTemplate;
        this.objMpr = objMpr;
        this.wikiFeignClient = wikiFeignClient;
    }

    public WikiResponseModel discover(String name) {
        return wikiFeignClient.discover(name);
    }

    public void saveToDatabase(String name, String discoveryStatus) {
        planetEntityRepository.save(new PlanetEntity(name, discoveryStatus));

    }

    public String discoverPlanet(String name) {
        return makeNostromoResponse(discover(name), name);
    }

    private String makeNostromoResponse(WikiResponseModel response, String planetName) {
        NostromoResponse nostromoResponse = new NostromoResponse();

        if (response.getError() != null) {
            logger.info("Required page doesn't exist");
            saveToDatabase(planetName, "fail");
            nostromoResponse.setError(response.getError().toString());
        } else {
            saveToDatabase(planetName, "success");
            logger.info("Created new entity :" + planetName);

            nostromoResponse.setResponse(response.getParse().toString());
        }

        try {
            return objMpr.writeValueAsString(nostromoResponse);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Error while generating JSON";
        }
    }

    public String getFullName() {
        String fullName = name + " on port " + port;
        logger.debug("Returned full name :" + fullName);
        return fullName;
    }

}
