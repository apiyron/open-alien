package ru.alien.shuttle.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.alien.shuttle.service.ShuttleService;

@RestController
public class ShuttleController {
    private String name;

    private ShuttleService shuttleService;

    public ShuttleController(ShuttleService shuttleService) {
        this.shuttleService = shuttleService;
    }
    
    @ApiOperation(value = "Get Shuttle name with tags", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved name"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping("/name")
    public String getName() {
        return shuttleService.getFullName();
    }

    @ApiOperation(value = "Ask Wikipedia for target and save response in DataBase", response = String.class)
    @GetMapping(value = "/discovery", params = {"target"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity getInfo(@RequestParam(value = "target") String name) {
        return new ResponseEntity<>(shuttleService.discoverPlanet(name), HttpStatus.OK);
    }
}

